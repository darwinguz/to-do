//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Darwin Guzmán on 9/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var tittleTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = tittleTextField.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
       
        var validacion = true
        if itemTitle.isEmpty {
            print("ERROR: Ingrese el title")
            validacion = false
        }
        if itemLocation.isEmpty {
            print("WARNING: Ingrese la location")
        }
        if itemDescription.isEmpty {
            print("ERROR: Ingrese la description")
            validacion = false
        }

        if !validacion {
            return
        }
        
        let item = Item(title: itemTitle, location: itemLocation, description: itemDescription)
        print("Paso...")
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
