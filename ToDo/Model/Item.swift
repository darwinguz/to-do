//
//  Item.swift
//  ToDo
//
//  Created by Darwin Guzmán on 9/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation

struct Item {
    let title:String
    let location:String
    let description:String
    
}
