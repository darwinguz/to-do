//
//  ViewController.swift
//  ToDo
//
//  Created by Darwin Guzmán on 8/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let itemManager = ItemManager()
    
    //delegate: parte grafica de la tabla e interaccion con el usuario
    //datasource: fuente de datos
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let item1 = Item(title: "ToDo 1", location: "Office", description: "Do something")
        let item2 = Item(title: "ToDo 2", location: "House", description: "Do something else")
        let item3 = Item(title: "ToDo 3", location: "Uni", description: "Do something else else")
        
        itemManager.toDoItems = [item1, item2, item3]
       
        let item4 = Item(title: "Done 1", location: "Narnia", description: "bla bla")
        
        itemManager.doneItems = [item4]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath)"
        if indexPath.section == 0 {
            cell.textLabel?.text = itemManager.toDoItems[indexPath.row].title
        }else{
            cell.textLabel?.text = itemManager.doneItems[indexPath.row].title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
}
